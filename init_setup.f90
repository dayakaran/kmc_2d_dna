subroutine run_constants

  use glbl_vars
  use event_vars
  implicit none

  integer :: seed

  !--------------------------------------------------------------------------
  !- Initialize the random number generator

  call get_a_random_seed ( seed )
  call initialize_prng   ( seed )
  
  !------------------------------------------------------------------------
  ! Total number of sites in system

  nsite          = length * height
  nmax_occupancy = nsite
  
  !------------------------------------------------------------------------
  ! Energy levels in system

  nlinks    = (nc*nsite)/2

  !------------------------------------------------------------------------
  ! Binary_tree parameters

  no_system_events = 4
  nmax_hop_events  = nc*no_particles
  nmax_link_events = nlinks
  
  size_event_list  = (nc*no_particles) + nlinks + no_system_events
  size_binary_tree = (2*size_event_list) - 1
    
  return
end subroutine run_constants




subroutine init_solvent

  use glbl_vars
  implicit none

  integer       :: nfluid_target, i, nfluid
  real(kind=dp) :: randf

  occ           = 0
  tag           = 0
  nfluid_target = no_particles
  nfluid        = 0
  nbonds        = 0
  links         = 0
  
  do 
     
     if (nfluid_target.eq.nfluid) exit

     call rngu ( randf ) 
     i = int(randf*nsite) + 1 

     if (occ(i).eq.0) then
        
        occ(i)            = 1
        nfluid            = nfluid + 1 
        tag(i)            = nfluid 
        colloids (nfluid) = particles (i, .false.)
        
     end if
     
  end do

  return 
end subroutine init_solvent



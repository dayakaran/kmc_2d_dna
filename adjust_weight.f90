subroutine adjust_weights(icycle)

  use glbl
  use tmmc_var
  implicit none

  integer             :: i,j,iter
  integer, intent(in) :: icycle
  
  real(kind = dp)     :: ctot
  
  P = 0.0_dp

  do i = nfluid_min, nfluid_max
     
     ctot = C(1,i) + C(2,i) + C(3,i)     
     if (ctot.ne.0.0_dp) then
        do j = 1,3
           P(j,i) = C(j,i)/ctot
        end do        
     end if     
  end do
  

  !-----------------------------------------------------------------------
  !~ We estimate all our probabilities relative to PIN(0) / PIN(nsystem)

  LPIN(nfluid_min) = 0.0_dp

  do i = nfluid_min, nfluid_max - 1        
     if ((P(1,i+1).ne.0.0_dp).and.(P(3,i).ne.0.0_dp)) then        
        LPIN(i+1) = LPIN(i) + log(P(3,i)) - log(P(1,i+1))        
     else        
        LPIN(i+1) = -karbit        
     end if     
  end do

  !-----------------------------------------------------------------------
  !~ Update weighting function

  eta = -1.0_dp*LPIN

  return  
end subroutine adjust_weights




subroutine data_collection_center(isweep)

  use glbl
  use tmmc_var
  implicit none
  include 'mpif.h'

  integer, intent(IN) :: isweep
  integer :: status(MPI_STATUS_SIZE)
  integer :: inode, ierr, i
  integer :: w_sta, w_end, w_rng
  real(kind = 8) :: stitch_value, sumsq

  if (mod(isweep,npr).eq.0) then        
     write(f_cchart,'(2I12,1F16.8)') isweep, nfluid, energy/real(nmax_occupancy)
  end if
  if (mod(isweep,npic).eq.0) call xyz_dump(f_vis)
  
  if (mod(isweep,nupdate).eq.0) then
     call adjust_weights(isweep)           
  end if
  
  if (mod(isweep, npr_pofn).eq.0) then
     
     call MPI_BARRIER (MPI_COMM_WORLD, ierr)
     if (idnode.eq.0) then
        
        LPIN_global = 0.0d0
        LPIN_buffer = 0.0d0
        do inode = 1, numnodes
           
           w_sta = window(1,inode)
           w_end = window(2,inode)
           w_rng = window(3,inode)

           if (inode.eq.1) then              
              LPIN_global(w_sta:w_end) = LPIN(:)              
           else
              LPIN_buffer = 0.0d0
              call MPI_RECV(LPIN_buffer(w_sta:w_end), w_rng, MPI_DOUBLE_PRECISION, &
                   inode - 1,11, MPI_COMM_WORLD, status, ierr)              
              stitch_value = LPIN_global(window(2, inode-1)) - LPIN_buffer(w_sta)
              LPIN_global(w_sta:w_end) = LPIN_buffer(w_sta:w_end) + stitch_value              
           end if
           
        end do
        
        sumsq = 0.0d0

        !- log(P(0)) th term is always same

        do i = 1, nmax_occupancy               
           if (sumsq.lt.real(nfluid_rng)) sumsq = sumsq + (abs(LPIN_global(i) - LPIN_old(i))/abs(LPIN_old(i)))               
        end do
        LPIN_old = LPIN_global
        
        write(f_conv,'(i16,1F16.8)')  isweep,sumsq/real(nmax_occupancy)  
        
        rewind(f_pofN_ccht)
        write(f_pofN_ccht,'(3A24)') 'Mu','rho','Log(P(N))'
        do i = 0, nmax_occupancy
           write(f_pofN_ccht,'(3F24.8)') cp,real(i)/real(nmax_occupancy),LPIN_global(i)
        end do
        write(f_pofN_ccht,*) ''

     else        
        call MPI_SSEND(LPIN(:), nfluid_rng, MPI_DOUBLE_PRECISION, 0, 11, MPI_COMM_WORLD,ierr)        
     end if
     
  end if

  return
end subroutine data_collection_center





subroutine data_collection_center_gcmc(isweep)

  use glbl
  use tmmc_var
  implicit none
  include 'mpif.h'

  integer, intent(IN) :: isweep
  integer             :: status(MPI_STATUS_SIZE)
  integer             :: inode, ierr, i
  real(kind = dp)     :: stitch_value, sumsq
  
  if (mod(isweep,npr).eq.0) then        
     write(f_cchart,'(2I12,1F16.8)') isweep, nfluid, energy/real(nmax_occupancy)
  end if

  if (isweep.gt.((4*nsweep)/5)) then

     if (mod(isweep,10).eq.0) then
        nmeas   = nmeas + 1
        
        avg_rho = avg_rho + (dble(nfluid) / dble(length*height))
        avg_ene = avg_ene + (energy / dble(length*height))
     end if
     
  end if

  if (mod(isweep,npic).eq.0) call xyz_dump(f_vis)
  
  return
end subroutine data_collection_center_gcmc




subroutine write_result_gcmc

  use glbl
  implicit none
  include 'mpif.h'

  integer             :: status(MPI_STATUS_SIZE)
  integer             :: inode, ierr, i

  real(kind=dp)       :: data_recv(3), data_send(3)
  
  call MPI_BARRIER (MPI_COMM_WORLD, ierr)

  if (idnode.eq.0) then
        
     do inode = 1, numnodes

        if (inode.eq.1) then              
           
           write(f_rslt,'(4F16.8)')  tstar, cp, avg_rho/dble(nmeas), avg_ene/dble(nmeas)
           
        else

           call MPI_RECV(data_recv, 3, MPI_DOUBLE_PRECISION, inode - 1,11, MPI_COMM_WORLD, status, ierr)
           write(f_rslt,'(4f16.8)')  tstar, data_recv(1), data_recv(2)/dble(nmeas), data_recv(3)/dble(nmeas)
           
        end if

     end do
        
  else
     
     data_send = (/ cp, avg_rho, avg_ene /)
     call MPI_SSEND(data_send, 3, MPI_DOUBLE_PRECISION, 0, 11, MPI_COMM_WORLD,ierr)
     
  end if
  
  return
end subroutine write_result_gcmc

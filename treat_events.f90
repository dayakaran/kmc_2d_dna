subroutine treat_particle_hop ( current_event )

  use glbl_vars
  use event_vars
  implicit none

  type(events), intent(in) :: current_event
  integer                  :: jumpsite, iparticle
  integer                  :: oldsite
  
  !- No change in energy

  iparticle                  = current_event % hero
  jumpsite                   = current_event % inf2
  oldsite                    = colloids(iparticle) % site
  colloids(iparticle) % site = jumpsite

  if (occ(jumpsite).eq.1) then
     write(*,*) 'Stop', current_event
     stop
  end if
  
  occ(jumpsite)              = 1
  tag(jumpsite)              = iparticle

  occ(oldsite)               = 0
  tag(oldsite)               = 0
  
  return
end subroutine treat_particle_hop





subroutine treat_bond_creation ( current_event )

  use glbl_vars
  use rate_vars
  use event_vars
  implicit none

  type(events), intent(in) :: current_event
  integer                  :: newlink
  integer                  :: ipart
  integer                  :: jpart
  
  energy            = energy - eps_bond  
  newlink           = current_event % hero
  
  links ( newlink ) = 1
  nbonds            = nbonds + 1

  !- Update bonding status of the two particles

  ipart             = tag(llist(1, newlink))
  jpart             = tag(llist(2, newlink))

  colloids (ipart) % is_bonded = .true.
  colloids (jpart) % is_bonded = .true.
  
  return
end subroutine treat_bond_creation







subroutine treat_bond_deletion ( current_event )

  use glbl_vars
  use rate_vars
  use event_vars
  implicit none

  type(events), intent(in) :: current_event
  integer                  :: dellink
  integer                  :: ipart, isite
  integer                  :: jpart, jsite
  integer                  :: snn
  integer                  :: k
  
  dellink           = current_event % hero
  energy            = energy + eps_bond

  links ( dellink ) = 0
  nbonds            = nbonds - 1

  isite             = llist ( 1, dellink )
  jsite             = llist ( 2, dellink )
  
  ipart             = tag ( isite )
  jpart             = tag ( jsite )

  !- Update bonding status of the particles involved
  
  snn = 0
  do k = 1, nc
     snn = snn + links ( slmap ( k, isite) )
  end do
  if (snn.eq.0) colloids(ipart) % is_bonded = .false.

  snn = 0
  do k = 1, nc
     snn = snn + links ( slmap ( k, jsite) )
  end do
  if (snn.eq.0) colloids(jpart) % is_bonded = .false.

  return
end subroutine treat_bond_deletion

subroutine loadparam 

  use glbl_vars
  use rate_vars
  use parse_module
  implicit none

  integer            :: io_error, nread=101, ierr

  character(len=200) :: record
  character(len=40 ) :: word

  logical            :: safe

  call set_defaults
  
  open(unit=nread,file='param.inp',status="old",iostat=io_error)  
  
  if (io_error.EQ.0) then

     do

        call get_line (safe, nread, record)
        if (.not.safe) exit
        call lower_case(record)
        call get_word  (record, word)

        if (word(1:6).eq.'length') then
           
           call get_word (record, word)
           length = nint(word_2_real(word))

        elseif (word(1:7).eq.'height') then
           
           call get_word (record, word)
           height = nint(word_2_real(word))
           
        elseif (word(1:5).eq.'tstar') then
           
           call get_word (record, word)
           tstar = word_2_real(word)

        elseif (word(1:5).eq.'npart') then
           
           call get_word (record, word)
           no_particles = nint(word_2_real(word))

        elseif (word(1:9).eq.'damkohler') then

           call get_word (record, word)
           damkohler_no = word_2_real(word)

        elseif (word(1:6).eq.'t_halt') then

           call get_word (record, word)
           next_halt_time = word_2_real(word)

        elseif (word(1:6).eq.'t_klik') then

           call get_word (record, word)
           next_selfie_time = word_2_real(word)

        elseif (word(1:6).eq.'t_meas') then

           call get_word (record, word)
           next_measur_time = word_2_real(word)          

        elseif (word(1:6).eq.'nsweep') then

           call get_word (record, word)
           nsweep = int(word_2_real(word))

        end if
     end do

  else
     
     write(*,*) 'ERROR: Parameters not loaded'
     call end_simulation
     
  end if
  
  close(nread)

  return
end subroutine loadparam


  

subroutine set_defaults

  use glbl_vars
  use rate_vars
  implicit none

  tstar            = 0.350_dp

  length           = 32
  height           = 32

  eps_bond         = 1.0_dp

  nsweep           = 10000
  
  next_selfie_time = 1000.0_dp
  next_measur_time = 100.0_dp
  next_halt_time   = 10000.0_dp      
  
  return
end subroutine set_defaults








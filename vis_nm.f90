subroutine vis (fileid)
 
  use glbl_vars
  implicit none
  
  integer, intent(in) :: fileid
  integer             :: i, j, is, ipart
  
  do j = 1, height
     do i = 1, length       
        is    = ((i-1) * height) + j
        ipart =  tag(is)

        if (ipart.eq.0) then
           write (fileid, '(i2)', advance='no') 0
        else
           if (colloids(ipart)%is_bonded) then
              write (fileid, '(i2)', advance='no') 2
           else
              write (fileid, '(i2)', advance='no') 1
           end if
        end if
        
     end do
     write(fileid,*) ''
  end do
  close(fileid)
  
  return
end subroutine vis


!------------------------------------------------------------------------------------------------
subroutine update_binary_tree(ilist)

  use event_vars
  implicit none

  integer, intent(in) :: ilist
  integer             :: child_left, child_right
  integer             :: parent,     start_value
  
  start_value = ilist + size_event_list - 1
  
  !------------------------------------------------------------------------------------------------
  !- Pick the best event for the ith particle 

  binary_tree ( start_value ) = event_list ( ilist )

  do
     
     if (mod(start_value,2).eq.0) then

        child_left  = start_value
        child_right = start_value + 1
        parent      = start_value / 2

     else

        child_left  = start_value - 1
        child_right = start_value 
        parent      = (start_value - 1)/ 2

     end if

     if (binary_tree(child_left) % event_time.le. binary_tree(child_right) % event_time) then    
        binary_tree(parent) = binary_tree(child_left)              
     else              
        binary_tree(parent) = binary_tree(child_right)              
     end if

     start_value = start_value/2    
     
     if (start_value.eq.1) exit
     
  end do

  return
end subroutine update_binary_tree

!------------------------------------------------------------------------------------------------




!------------------------------------------------------------------------------------------------

subroutine construct_binary_tree

  use kinds_f90
  use event_vars
  implicit none

  integer :: index,      start_value, range
  integer :: child_left, child_right, parent

  start_value = size_event_list
  do 

     if (mod(start_value,2).eq.0) then
        start_value = start_value/2
        range = start_value     
     else
        start_value = (start_value + 1)/2
        range = start_value 
     end if

     do index = 0, range - 1
        
        parent = start_value + index
        if (binary_tree(parent) % event_time.lt.0.0_dp) then
           
           child_left  = (2*parent)
           child_right = (2*parent) + 1
           
           if (binary_tree(child_left) % event_time.le. binary_tree(child_right) % event_time) then    
              binary_tree(parent) = binary_tree(child_left)              
           else              
              binary_tree(parent) = binary_tree(child_right)              
           end if
           
        end if

     end do
     if (start_value.eq.1) exit
     
  end do
  
  return
end subroutine construct_binary_tree




!------------------------------------------------------------------------------------------------

subroutine initialize_cbt
  
  use rate_vars
  use event_vars
  implicit none

  integer              :: ilist, iparticle 
  type(events)         :: empty_event

  empty_event = events (0, 0, 0, 0, .true., 0.0_dp, -1.0_dp)

  do ilist = 1, size_event_list - 1
     binary_tree(ilist) = empty_event
  end do

  do ilist = size_event_list, size_binary_tree 

     iparticle             = ilist - size_event_list + 1
     binary_tree ( ilist ) = event_list ( iparticle )
     
  end do
  
  return
end subroutine initialize_cbt

!------------------------------------------------------------------------------------------------

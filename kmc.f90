subroutine kmc

  use glbl_vars
  use event_vars
  use rate_vars
  implicit none


  integer      :: ievent, id
  type(events) :: current_event
  type(events) :: next_event
  type(events) :: previous_event

  ievent = 0
  call open_vis_file (id)
  call vis(id)
  
  do

     next_event     = binary_tree(1)
     current_event  = next_event
     previous_event = current_event

     global_time    = current_event % event_time

     !write(*,*) current_event

     select case (current_event % event_type)

     case ( EVENT_HOP_NEI )

        call treat_particle_hop   ( current_event )
        call particle_hop_updates ( current_event )
        ievent = ievent + 1

     case ( EVENT_BND_CRE )

        call treat_bond_creation   ( current_event )
        call bond_creation_updates ( current_event )
        ievent = ievent + 1
       
     case ( EVENT_BND_DEL )

        call treat_bond_deletion   ( current_event )
        call bond_deletion_updates ( current_event )
        ievent = ievent + 1

     case ( EVENT_TYPE_MEAS )

        call measurement
        event_list(nmax_hop_events + nmax_link_events + 3) = events (0, 0, 0, EVENT_TYPE_MEAS, .false., 0.0_dp, next_measur_time + global_time)
        call update_binary_tree(nmax_hop_events + nmax_link_events + 3)

     case ( EVENT_TYPE_KLIK )

        call open_vis_file (id)
        call vis (id)
        event_list(nmax_hop_events + nmax_link_events + 1) = events (0, 0, 0, EVENT_TYPE_KLIK, .false., 0.0_dp,next_selfie_time + global_time)
        call update_binary_tree(nmax_hop_events + nmax_link_events + 1)
        
     case ( EVENT_TYPE_HALT )
        
        return
        
     case (0)
        
        call end_simulation(0)
        
     end select
     
  end do

  return
end subroutine kmc








subroutine particle_hop_updates ( current_event )

  use glbl_vars
  use rate_vars
  use event_vars
  implicit none

  type(events), intent(in)   :: current_event
  type(events)               :: new_event

  integer                    :: rdirec, rsite
  integer                    :: iparticle
  integer                    :: event_index

  integer                    :: osite, nesite
  integer                    :: ineigh, ilink

  integer                    :: jparticle, jdirec
  
  real(kind = dp)            :: time_of_event
  real(kind = dp), external  :: exp_rn

  integer, external          :: get_reverse_direction
  
  iparticle = current_event % hero
  rdirec    = current_event % inf1
  rsite     = current_event % inf2

  do ineigh = 1, nc
     
     event_index = (nc*(iparticle-1)) + ineigh 
     
     if ( ineigh.eq.rdirec ) then

        call get_new_hop_event  ( iparticle, rdirec, event_list(event_index), new_event )
        event_list( event_index ) = new_event
        call update_binary_tree ( event_index )
        
     else

        call update_hop_event   ( iparticle, ineigh, event_list(event_index), new_event )
        event_list( event_index ) = new_event
        call update_binary_tree ( event_index )
        
     end if
     
  end do

  osite = nlist(get_reverse_direction (rdirec), rsite)

  !- Particles who were planning to hop to rsite can no longer hop
  
  do ineigh = 1, nc

     nesite = nlist (ineigh, rsite)
     if (nesite.eq.osite) cycle

     if (occ(nesite).eq.1) then

        jparticle = tag(nesite)
        jdirec    = get_reverse_direction (ineigh)

        event_index = ((nc*(jparticle-1)) + jdirec)
        call update_hop_event   ( jparticle, jdirec, event_list(event_index), new_event )
        event_list( event_index ) = new_event
        call update_binary_tree ( event_index )
        
     end if
     
  end do
  
  
  do ineigh = 1, nc

     ilink       = slmap ( ineigh, rsite )
     event_index = nmax_hop_events + ilink
     call update_link ( ilink, event_list(event_index), new_event )
     event_list ( event_index ) = new_event
     call update_binary_tree ( event_index )

  end do

  osite = nlist(get_reverse_direction (rdirec), rsite)

  do ineigh = 1, nc
    
     if (ineigh.eq.rdirec) cycle
     
     ilink = slmap ( ineigh, osite )
     event_index = nmax_hop_events + ilink
     call update_link ( ilink, event_list(event_index), new_event )
     event_list(event_index) = new_event
     call update_binary_tree ( event_index )
     !- Check these

  end do

  !- 7 links are affected in the process
  !- How do we update those events
  
  return
end subroutine particle_hop_updates







subroutine bond_creation_updates ( current_event )

  use glbl_vars
  use rate_vars
  use event_vars
  implicit none

  type ( events ) , intent(in) :: current_event
  type ( events )              :: new_event
  
  integer                      :: ilink
  integer                      :: ipart
  integer                      :: jpart
  integer                      :: ineigh
  integer                      :: event_index
  
  !- Get new bond deletion event  
  ilink       = current_event % hero
  event_index = nmax_hop_events + ilink
  call estimate_bond_deletion_event ( ilink, current_event, new_event )
  event_list ( event_index ) = new_event
  call update_binary_tree ( event_index )

  !- 4 events need updates here
  ipart     = current_event % inf1

  do ineigh = 1, nc
     
     event_index = (nc*(ipart-1)) + ineigh      
     call update_hop_event ( ipart, ineigh, event_list(event_index), new_event )
     event_list ( event_index ) = new_event
     call update_binary_tree ( event_index )
        
  end do

  !- 4 events need updates here

  jpart     = current_event % inf2

  do ineigh = 1, nc
     
     event_index = (nc*(jpart-1)) + ineigh      
     call update_hop_event ( jpart, ineigh, event_list(event_index), new_event )
     event_list ( event_index ) = new_event
     call update_binary_tree ( event_index )
         
  end do

  
  return
end subroutine bond_creation_updates







subroutine bond_deletion_updates ( current_event )

  use glbl_vars
  use rate_vars
  use event_vars
  implicit none
  
  type ( events ) , intent(in) :: current_event
  type ( events )              :: new_event
  
  integer                      :: ilink
  integer                      :: ipart
  integer                      :: jpart
  integer                      :: ineigh
  integer                      :: event_index
  
  
  ilink     = current_event % hero
  
  ipart     = current_event % inf1
  jpart     = current_event % inf2

  call estimate_bond_formation_event ( ilink, current_event, new_event )
  event_index = nmax_hop_events + ilink
  event_list ( event_index ) = new_event
  call update_binary_tree ( event_index )
  
  ! get a new bond creation event for ilink and update binary tree
  ! update hopping events for ipart and jpart ; update the binary tree

  !- 4 events need updates here
  ipart     = current_event % inf1

  do ineigh = 1, nc
     
     event_index = (nc*(ipart-1)) + ineigh      
     call update_hop_event ( ipart, ineigh, event_list(event_index), new_event )
     event_list ( event_index ) = new_event
     call update_binary_tree ( event_index )
        
  end do

  !- 4 events need updates here

  jpart     = current_event % inf2

  do ineigh = 1, nc
     
     event_index = (nc*(jpart-1)) + ineigh      
     call update_hop_event ( jpart, ineigh, event_list(event_index), new_event )
     event_list ( event_index ) = new_event
     call update_binary_tree ( event_index )
         
  end do
 
  return
end subroutine bond_deletion_updates








function get_reverse_direction (ix) result(rx)

  implicit none
  integer, intent(in) :: ix
  integer             :: rx
  
  select case (ix)
  case(1)
     rx = 2
  case(2)
     rx = 1
  case(3)
     rx = 4
  case(4)
     rx = 3
  end select
  
  return
end function get_reverse_direction

subroutine lattice
  
  use glbl_vars
  implicit none
  
  integer :: i,j,k,is
  integer :: i1, i2, j1, j2
  integer :: ierr,all_error,isite
  integer :: ilink
  
  !--------------------------------------------------------------------------
  !~ Makes a simple cubic lattice
  
  isite = 0
  
  do i = 1, length
     do j = 1, height
        
        is = ((i-1) * height) + j
        isite = isite + 1
        ilist(isite) = is        
        ix(is) = i
        iy(is) = j

        i1 = i + 1
        if(i1.gt.length) i1 = 1
        i2 = i - 1
        if(i2.lt.1) i2 = length
        j1 = j + 1
        if(j1.gt.height) j1 = 1
        j2 = j - 1
        if(j2.lt.1) j2 = height

        !--------------------------------------------------------------------------
        ! list of nearest neighbors

        nlist(1,is) = ((i1-1) * height) + j
        nlist(2,is) = ((i2-1) * height) + j
        nlist(3,is) = ((i-1) * height) + j2
        nlist(4,is) = ((i-1) * height) + j1

     end do
  end do


  ilink = 0
  
  do isite = 1, nsite

     do j = 1, nc, 2
     
        ilink = ilink + 1

        llist(1, ilink) = isite
        llist(2, ilink) = nlist (j, isite)

        slmap ( j,   isite)          = ilink
        slmap ( j+1, nlist(j,isite)) = ilink
        
     end do
     
  end do

 
  return  
end subroutine lattice

























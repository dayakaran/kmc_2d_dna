#---------------------------------------------#
#
# List of all the object files
#---------------------------------------------#
OBJECT_SER = constants.o mtprng.o parse_module.o var_definitions.o mcmain.o loadparam.o kmc.o energy.o init_setup.o treat_events.o lat_make.o event_calender.o event_scheduler.o serial_routines.o gcmc.o vis_nm.o
#
OBJECT_PAR = constants_p.o mtprng_p.o parse_module_p.o var_definitions_p.o mcmain_p.o loadparam_p.o kmc_p.o energy_p.o init_setup_p.o treat_events_p.o lat_make_p.o event_calender_p.o event_scheduler_p.o parallel_routines.o gcmc_p.o vis_nm_p.o
#---------------------------------------------#
#
# Optimization options
#---------------------------------------------#
#---------------------------------------------#
#
GSL_LIBCALL= -lgsl -lgslcblas 
#
#----------------------------------------------#
#FC_FLAG = -O2 -ffree-line-length-none #-Mbounds #-fbounds-check  #-march=opteron 
FC_FLAG = -O2 -ffree-line-length-none -fbounds-check  -g #-march=opteron 
#
#---------------------------------------------#
#
# Specifying Honourable Sir Compiler
#---------------------------------------------#
#
COMPIL     = gfortran
COMPIL_PAR = mpif90
#
all: $(OBJECT_SER)
	$(COMPIL) -o kmc_ser $(FC_FLAG) $(OBJECT_SER) 
	chmod +x kmc_ser
#	cp lgc_tmmc_2D ~/bin/lgc_tmmc_par_2D_suc
ser: $(OBJECT_SER)
	$(COMPIL) -o kmc_ser $(FC_FLAG) $(OBJECT_SER) 
	chmod +x kmc_ser
#
par: $(OBJECT_PAR)
	$(COMPIL_PAR) -o kmc_par $(FC_FLAG) $(OBJECT_PAR) 
	chmod +x kmc_par
#
constants.o: constants.f90
	$(COMPIL) -c $(FC_FLAG) constants.f90 
#
constants_p.o: constants.f90
	$(COMPIL_PAR) -c $(FC_FLAG) constants.f90 -o constants_p.o
#
mcmain.o: mcmain.f90
	$(COMPIL) -c $(FC_FLAG) mcmain.f90
#
mcmain_p.o: mcmain.f90
	$(COMPIL_PAR) -c $(FC_FLAG) mcmain.f90 -o mcmain_p.o
#
loadparam.o: loadparam.f90
	$(COMPIL) -c $(FC_FLAG) loadparam.f90
#
loadparam_p.o: loadparam.f90
	$(COMPIL_PAR) -c $(FC_FLAG) loadparam.f90 -o loadparam_p.o
#
kmc.o: kmc.f90
	$(COMPIL) -c $(FC_FLAG) kmc.f90
#
kmc_p.o: kmc.f90
	$(COMPIL_PAR) -c $(FC_FLAG) kmc.f90 -o kmc_p.o
#
lat_make.o: lat_make.f90
	$(COMPIL) -c $(FC_FLAG) lat_make.f90
#
lat_make_p.o: lat_make.f90
	$(COMPIL_PAR) -c $(FC_FLAG) lat_make.f90 -o lat_make_p.o
#
event_calender.o: event_calender.f90
	$(COMPIL) -c $(FC_FLAG) event_calender.f90
#
reset_event_calender.o: reset_event_calender.f90
	$(COMPIL) -c $(FC_FLAG) reset_event_calender.f90
#
event_calender_p.o: event_calender.f90
	$(COMPIL_PAR) -c $(FC_FLAG) event_calender.f90 -o event_calender_p.o
#
reset_event_calender_p.o: reset_event_calender.f90
	$(COMPIL_PAR) -c $(FC_FLAG) reset_event_calender.f90 -o reset_event_calender_p.o
#
event_scheduler.o: event_scheduler.f90
	$(COMPIL) -c $(FC_FLAG) event_scheduler.f90 
#
event_scheduler_p.o: event_scheduler.f90
	$(COMPIL_PAR) -c $(FC_FLAG) event_scheduler.f90 -o event_scheduler_p.o 
#
vis_nm.o: vis_nm.f90
	$(COMPIL) -c $(FC_FLAG) vis_nm.f90
#
vis_nm_p.o: vis_nm.f90
	$(COMPIL_PAR) -c $(FC_FLAG) vis_nm.f90 -o vis_nm_p.o
#
energy.o: energy.f90
	$(COMPIL) -c $(FC_FLAG) energy.f90
#
energy_p.o: energy.f90
	$(COMPIL_PAR) -c $(FC_FLAG) energy.f90 -o energy_p.o
#
parse_module.o: parse_module.f90
	$(COMPIL) -c $(FC_FLAG) parse_module.f90
#
parse_module_p.o: parse_module.f90
	$(COMPIL_PAR) -c $(FC_FLAG) parse_module.f90 -o parse_module_p.o
#
mtprng.o: mtprng.f90
	$(COMPIL) -c $(FC_FLAG) mtprng.f90
#
mtprng_p.o: mtprng.f90
	$(COMPIL_PAR) -c $(FC_FLAG) mtprng.f90 -o mtprng_p.o
#
init_setup.o: init_setup.f90
	$(COMPIL) -c $(FC_FLAG) init_setup.f90
#
init_setup_p.o: init_setup.f90
	$(COMPIL_PAR) -c $(FC_FLAG) init_setup.f90 -o init_setup_p.o
#
treat_events.o: treat_events.f90
	$(COMPIL) -c $(FC_FLAG) treat_events.f90
#
treat_events_p.o: treat_events.f90
	$(COMPIL_PAR) -c $(FC_FLAG) treat_events.f90 -o treat_events_p.o
#
var_definitions.o: var_definitions.f90
	$(COMPIL) -c $(FC_FLAG) var_definitions.f90 
#
var_definitions_p.o: var_definitions.f90
	$(COMPIL_PAR) -c $(FC_FLAG) var_definitions.f90 -o var_definitions_p.o
#
serial_routines.o: serial_routines.f90
	$(COMPIL) -c $(FC_FLAG) serial_routines.f90
#
gcmc.o: gcmc.f90
	$(COMPIL) -c $(FC_FLAG) gcmc.f90
#
gcmc_p.o: gcmc.f90
	$(COMPIL_PAR) -c $(FC_FLAG) gcmc.f90 -o gcmc_p.o
#
parallel_routines.o: parallel_routines.f90
	$(COMPIL_PAR) -c $(FC_FLAG) parallel_routines.f90
#
clean:
	rm *.o kmc_* *mod *~

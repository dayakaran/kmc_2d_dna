subroutine build_future_events_calender 

  use glbl_vars
  use event_vars
  use rate_vars

  implicit none

  integer             :: isite, ibond, ievent
  integer             :: event_index, ineigh
  type(events)        :: next_event, dummy_event
  integer             :: iparticle

  !-------------------------------------------------------------------------
  !- INITIALIZE THE FUTURE EVENTS LIST HERE

  global_time      = 0.0_dp
  dummy_event      = events (0, 0, 0, 0, .true., 0.0_dp, 0.0_dp) 

  do iparticle = 1, no_particles

     do ineigh = 1, nc
        event_index = (nc*(iparticle-1))+ineigh 
        call compute_particle_hop_events ( iparticle, ineigh, dummy_event,  next_event )
        event_list ( event_index ) = next_event
     end do
     
  end do
  
  do ibond = 1, nmax_link_events  !- 1 event for each link
     call compute_link_events ( ibond, dummy_event, next_event )
     event_list ( nmax_hop_events + ibond ) = next_event
  end do
    
  do ievent = 1, no_system_events

     if (ievent.eq.1) then
        event_list ( nmax_hop_events + nmax_link_events + ievent ) = events ( 0, 0, 0, EVENT_TYPE_KLIK, .false., 0.0_dp, next_selfie_time ) 
     elseif (ievent.eq.2) then
        event_list ( nmax_hop_events + nmax_link_events + ievent ) = events ( 0, 0, 0, EVENT_TYPE_HALT, .false., 0.0_dp, next_halt_time )
     elseif (ievent.eq.3) then
        event_list ( nmax_hop_events + nmax_link_events + ievent ) = events ( 0, 0, 0, EVENT_TYPE_MEAS, .false., 0.0_dp, next_measur_time )
     else
        event_list ( nmax_hop_events + nmax_link_events + ievent ) = events ( 0, 0, 0, EVENT_TYPE_MEAS, .false., 0.0_dp, INF_TIME )
     end if

  end do

  call initialize_bond_site_trackers  
  call initialize_cbt
  call construct_binary_tree

  return
end subroutine build_future_events_calender






subroutine compute_particle_hop_events ( iparticle, ineigh, old_event, hopping_event )

  use glbl_vars
  use rate_vars
  use event_vars
  implicit none

  integer,      intent(in)   :: iparticle
  integer,      intent(in)   :: ineigh

  type(events), intent(in)   :: old_event
  type(events), intent(out)  :: hopping_event
  
  type(events)               :: impossible_hop_event
  
  integer                    :: nesite
  integer                    :: idirec, isite
  integer                    :: jparticle

  real(kind = dp)            :: time_of_event
  real(kind = dp), external  :: exp_rn
  
  impossible_hop_event = events ( iparticle, 0, 0, EVENT_HOP_NEI, .true., 0.0_dp, global_time + INF_TIME )

  if (colloids(iparticle) % is_bonded) then
     
     hopping_event = impossible_hop_event

     if (.not.old_event%is_stuck) then
        track_hops_ze ( (nc*(iparticle-1)) + ineigh ) = tracker( old_event % event_rate, old_event % event_time, global_time, .false. )
     end if

  else

     isite  = colloids(iparticle) % site
     nesite = nlist (ineigh, isite)
     
     if ( occ(nesite).eq.1 ) then
        
        hopping_event =  impossible_hop_event

        !- This probably will also never occur
        if (.not.old_event%is_stuck) then
           track_hops_ze ( (nc*(iparticle-1)) + ineigh ) = tracker( old_event % event_rate, old_event % event_time, global_time, .false. )
        end if

     else

        hopping_event = events ( iparticle, ineigh, nesite, EVENT_HOP_NEI, .false., rate_hop, global_time + exp_rn(rate_hop))
        
     end if
     
  end if


  return
end subroutine compute_particle_hop_events








subroutine get_new_hop_event ( iparticle, ineigh, old_event, updated_event )

  use glbl_vars
  use rate_vars
  use event_vars
  implicit none

  integer,      intent(in)  :: iparticle, ineigh
  type(events), intent(in)  :: old_event
  type(events), intent(out) :: updated_event

  type(events)              :: impossible_hop_event

  integer                   :: isite, jsite
  
  real(kind=dp)             :: exp_rn

  !- This is called only when the particles is not bonded 
  
  isite                = colloids ( iparticle     ) % site
  jsite                = nlist    ( ineigh, isite )  

  impossible_hop_event = events ( iparticle, ineigh, jsite, EVENT_HOP_NEI, .true., 0.0_dp, global_time + INF_TIME )
  
  if (occ(jsite).eq.0) then     

     updated_event     = events ( iparticle, ineigh, jsite, EVENT_HOP_NEI, .false., rate_hop, global_time + exp_rn(rate_hop))
     
  else

     updated_event = impossible_hop_event
     !_ This routine is called only after a successful hop and the random number is used up
     track_hops_ze ( (nc*(iparticle-1)) + ineigh ) % is_virgin = .true.

  end if
  
  !if (iparticle.eq.34) write(*,*) updated_event, 'N', colloids(34)
  
  return
end subroutine get_new_hop_event





subroutine update_hop_event ( iparticle, ineigh, old_event, updated_event )

  use glbl_vars
  use rate_vars
  use event_vars
  implicit none

  integer,      intent(in)  :: iparticle, ineigh
  type(events), intent(in)  :: old_event
  type(events), intent(out) :: updated_event

  type(events)              :: impossible_hop_event
  type(tracker)             :: hop_event_tracker

  integer                   :: isite, jsite
  
  real(kind=dp)             :: exp_rn
  
  impossible_hop_event = events ( iparticle, ineigh, 0, EVENT_HOP_NEI, .true., 0.0_dp, global_time + INF_TIME )
  
  if (colloids(iparticle) % is_bonded) then
     
     updated_event = impossible_hop_event
     if (.not.old_event%is_stuck) then
        track_hops_ze ( (nc*(iparticle-1)) + ineigh ) = tracker( old_event % event_rate, old_event % event_time, global_time, .false. )
     end if

  else

     isite                = colloids(iparticle) % site
     jsite                = nlist(ineigh, isite)  

     if (occ(jsite).eq.0) then
             
        updated_event     = events ( iparticle, ineigh, jsite, EVENT_HOP_NEI, .false., rate_hop, 0.0_dp)

        if (old_event % is_stuck) then
        
           hop_event_tracker = track_hops_ze ( (nc*(iparticle-1)) + ineigh )

           if (hop_event_tracker % is_virgin) then
              
              updated_event % event_time = global_time + exp_rn ( rate_hop )
              track_hops_ze ( (nc*(iparticle-1)) + ineigh ) % is_virgin = .false.
              
           else
              
              !write(*,*) hop_event_tracker, 'Tracker issue'
              updated_event % event_time = (( hop_event_tracker % tau - hop_event_tracker % time ) * (hop_event_tracker % rate / rate_hop)) + global_time !- This ratio is 1.0

           end if

        else
           updated_event % event_time = (( old_event % event_time - global_time ) * ( old_event % event_rate / rate_hop)) + global_time
        end if

     else

        updated_event = impossible_hop_event
        if (.not.old_event%is_stuck) then
           track_hops_ze ( (nc*(iparticle-1)) + ineigh ) = tracker( old_event % event_rate, old_event % event_time, global_time, .false. )
        end if
     
     end if
     
  end if

  !if (iparticle.eq.34) write(*,*) updated_event, 'U'
  
  return
end subroutine update_hop_event








subroutine compute_link_events ( rlink, old_event, link_event )

  use glbl_vars
  use event_vars
  implicit none

  integer,      intent(in)  :: rlink
  type(events), intent(in)  :: old_event
  type(events), intent(out) :: link_event
  
  
  if ( links(rlink).eq.0 ) then
     call estimate_bond_formation_event ( rlink, old_event, link_event )
  else
     call estimate_bond_deletion_event  ( rlink, old_event, link_event )
  end if
  
  return
end subroutine compute_link_events






subroutine update_link ( rlink, old_event, updated_event )

  use glbl_vars
  use event_vars
  implicit none

  integer,      intent(in)  :: rlink
  type(events), intent(in)  :: old_event
  type(events), intent(out) :: updated_event
  
  if ( links(rlink).eq.0 ) then
     call update_bond_formation_event ( rlink, old_event, updated_event )
  else
     ! This cannot happen. Bonded particles cannot hop, so this event cannot follow a hop event
     call update_bond_deletion_event  ( rlink, old_event, updated_event )
     write(*,*) 'Unusual', old_event
  end if

  return
end subroutine update_link





subroutine estimate_bond_formation_event ( rlink, old_event, bonding_event )

  use glbl_vars
  use rate_vars
  use event_vars
  implicit none

  integer,      intent(in)   :: rlink
  type(events), intent(in)   :: old_event
  type(events), intent(out)  :: bonding_event
  
  type(events)               :: impossible_bnd_event

  integer                    :: isite, jsite
  integer                    :: socc,  nocc
  
  integer                    :: ineigh, nesite
  integer                    :: idirec
  integer                    :: jparticle

  real(kind = dp)            :: time_of_event
  real(kind = dp), external  :: exp_rn
  
  isite                = llist ( 1, rlink )
  jsite                = llist ( 2, rlink )
  
  socc                 = occ   ( isite ) 
  nocc                 = occ   ( jsite )

  impossible_bnd_event = events ( rlink, 0, 0, EVENT_BND_CRE, .true., 0.0_dp, global_time+INF_TIME )
  
  if ( (socc.eq.1).and.(nocc.eq.1) ) then
     time_of_event = exp_rn ( rate_bond_formation )
     bonding_event = events ( rlink, tag(isite), tag(jsite), EVENT_BND_CRE, .false., rate_bond_formation, global_time + time_of_event )
  else
     bonding_event = impossible_bnd_event
     if (.not.old_event % is_stuck) then
        track_links_ze (rlink ) = tracker ( old_event % event_rate, old_event % event_time, global_time, .false. )
     end if
     !- Tracker should be alerted when schedule a rate = 0.0 event     
  end if
  
  return
end subroutine estimate_bond_formation_event




subroutine update_bond_formation_event ( rlink, old_event, bonding_event )

  use glbl_vars
  use rate_vars
  use event_vars
  implicit none

  integer,      intent(in)   :: rlink
  type(events), intent(in)   :: old_event
  type(events), intent(out)  :: bonding_event
  
  type(events)               :: impossible_bnd_event

  type(tracker)              :: link_event_tracker
  
  integer                    :: isite, jsite
  integer                    :: socc,  nocc
  
  integer                    :: ineigh, nesite
  integer                    :: idirec
  integer                    :: jparticle

  real(kind = dp)            :: time_of_event
  real(kind = dp), external  :: exp_rn
 
  isite                = llist ( 1, rlink )
  jsite                = llist ( 2, rlink )
  
  socc                 = occ   ( isite ) 
  nocc                 = occ   ( jsite )
  
  impossible_bnd_event = events ( rlink, 0, 0, EVENT_BND_CRE, .true., 0.0_dp, global_time+INF_TIME )
  
  if ( (socc.eq.1).and.(nocc.eq.1) ) then

     bonding_event = events ( rlink, tag(isite), tag(jsite), EVENT_BND_CRE, .false., rate_bond_formation, 0.0_dp )
     
     if (old_event % is_stuck ) then

        link_event_tracker = track_links_ze ( rlink )

        if (link_event_tracker % is_virgin) then

           time_of_event                      = exp_rn ( rate_bond_formation )
           bonding_event % event_time         =  global_time + time_of_event 
           track_links_ze (rlink) % is_virgin = .false.
           
        else

           time_of_event              = (( link_event_tracker % tau - link_event_tracker % time ) * ( link_event_tracker % rate / rate_bond_formation))
           bonding_event % event_time =  global_time + time_of_event 
           
        end if

     else

        bonding_event % event_time = (( old_event % event_time - global_time ) * ( old_event % event_rate / rate_bond_formation)) + global_time 
        
     end if
     
  else
     bonding_event = impossible_bnd_event
     if (.not.old_event % is_stuck) then
        track_links_ze (rlink ) = tracker ( old_event % event_rate, old_event % event_time, global_time, .false. )
     end if
     !- Tracker should be alerted when schedule a rate = 0.0 event     
  end if
  
  return
end subroutine update_bond_formation_event







subroutine estimate_bond_deletion_event ( rlink, old_event, bonding_event )

  use glbl_vars
  use rate_vars
  use event_vars
  implicit none

  integer,      intent(in)   :: rlink
  type(events), intent(in)   :: old_event
  type(events), intent(out)  :: bonding_event

  integer                    :: isite, jsite
  
  real(kind = dp)            :: time_of_event
  real(kind = dp), external  :: exp_rn
  
  isite         = llist ( 1, rlink )
  jsite         = llist ( 2, rlink )

  !- Check this part  
  
  time_of_event = exp_rn ( rate_bond_deletion )
  bonding_event = events ( rlink, tag(isite), tag(jsite), EVENT_BND_DEL, .false., rate_bond_deletion, global_time + time_of_event )
  
  return
end subroutine estimate_bond_deletion_event




subroutine update_bond_deletion_event ( rlink, old_event, bonding_event )

  use glbl_vars
  use rate_vars
  use event_vars
  implicit none

  integer,      intent(in)   :: rlink
  type(events), intent(in)   :: old_event
  type(events), intent(out)  :: bonding_event

  integer                    :: isite, jsite
  
  integer                    :: jparticle

  real(kind = dp)            :: time_of_event
  real(kind = dp), external  :: exp_rn

  write(*,*) old_event, 'This is the old event'
  write(*,*) 'This cannot happen', links(rlink)
  
  ! This cannot happen. Bonded particles cannot hop, so this event cannot follow a hop event
  
  ! isite         = llist ( 1, rlink )
  ! jsite         = llist ( 2, rlink )
  
  ! socc          = occ   ( isite ) 
  ! nocc          = occ   ( jsite )
  
  ! time_of_event = exp_rn ( rate_bond_deletion )
  ! bonding_event = events ( rlink, tag(isite), tag(jsite), EVENT_BND_DEL, rate_bond_deletion, .false., global_time + time_of_event )
  
  return
end subroutine update_bond_deletion_event






subroutine initialize_bond_site_trackers

  use glbl_vars
  use event_vars
  use rate_vars
  implicit none

  integer :: ibond, ipart
  integer :: isite, jsite
  integer :: socc,  nocc
  integer :: j
  
  do ipart = 1, no_particles

     isite = colloids(ipart) % site

     if (colloids(ipart) % is_bonded) then

        track_hops_ze ( (nc*(ipart-1)) + 1 ) = tracker ( 0.0_dp, -1.0_dp, -1.0_dp, .true. )
        track_hops_ze ( (nc*(ipart-1)) + 2 ) = tracker ( 0.0_dp, -1.0_dp, -1.0_dp, .true. )
        track_hops_ze ( (nc*(ipart-1)) + 3 ) = tracker ( 0.0_dp, -1.0_dp, -1.0_dp, .true. )
        track_hops_ze ( (nc*(ipart-1)) + 4 ) = tracker ( 0.0_dp, -1.0_dp, -1.0_dp, .true. )
        
     else

        do j = 1, nc
           if (occ ( nlist(j, isite)).eq.0) then
              track_hops_ze ( (nc*(ipart-1)) + j ) = tracker ( 0.0_dp, -1.0_dp, -1.0_dp, .false. )
           else
              track_hops_ze ( (nc*(ipart-1)) + j ) = tracker ( 0.0_dp, -1.0_dp, -1.0_dp, .true. )
           end if
        end do
        
     end if
     !- Depends on neighborhood if a random number has ever been utilized
     
  end do

  do ibond = 1, nlinks

     if (links(ibond).eq.0) then
     
        isite  = llist(1, ibond)
        jsite  = llist(2, ibond)
        
        socc   = occ(isite) 
        nocc   = occ(jsite)

        if ((socc.eq.1).and.(nocc.eq.1)) then
           track_links_ze (ibond) = tracker ( 0.0_dp, -1.0_dp, -1.0_dp, .false. )
        else
           track_links_ze (ibond) = tracker ( 0.0_dp, -1.0_dp, -1.0_dp, .true. )        
        end if
        
     else
        track_links_ze (ibond) = tracker ( 0.0_dp, -1.0_dp, -1.0_dp, .false. )
     end if
     
  end do
    
  return
end subroutine initialize_bond_site_trackers




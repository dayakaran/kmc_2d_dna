subroutine init_simulation

  use glbl_vars
  implicit none  
  include 'mpif.h'

  integer            :: ierr
  character(len=24)  :: folname
  
  call MPI_INIT ( ierr )  
  call MPI_COMM_SIZE ( MPI_COMM_WORLD, numnodes, ierr )  
  call MPI_COMM_RANK ( MPI_COMM_WORLD, idnode  , ierr )

  if (idnode.eq.0) then

     open ( unit=f_info, file='siminfo.dat', status='unknown' )
     open ( unit=g_fil1, file='time.dat',    status='unknown' )
     open ( unit=g_fil2, file='nfree.dat',   status='unknown' )
     open ( unit=g_fil3, file='nlinks.dat',  status='unknown' )
     open ( unit=g_fil4, file='c_max.dat',   status='unknown' )
     open ( unit=g_fil7, file='energy.dat',  status='unknown' )

  end if

  call get_foldername ( idnode+1,folname )
  call system ("mkdir "//trim(folname))
  call system ("mkdir "//trim(folname)//"/pics")

  open ( unit=f_fil2, file=trim(folname)//'/nfree.dat',   status='unknown' )
  open ( unit=f_fil3, file=trim(folname)//'/nlinks.dat',  status='unknown' )
  open ( unit=f_fil4, file=trim(folname)//'/c_max.dat',   status='unknown' )
  open ( unit=f_fil7, file=trim(folname)//'/energy.dat',  status='unknown' )

  return  
end subroutine init_simulation





subroutine open_vis_file(rid)

  use glbl_vars, only : f_fil8, idnode
  implicit none

  integer, intent(out) :: rid
  integer, save        :: id = 0

  character(len=24)    :: fname
  character(len=24)    :: folname
  
  id  = id + 1

  call get_foldername ( idnode+1,folname )
  call get_filename   ( id, fname )
  open ( unit=f_fil8, file=trim(folname)//trim(fname),   status='unknown' )
  rid = f_fil8
  
  return
end subroutine open_vis_file






subroutine get_filename (i, fname)

  use glbl_vars
  implicit none  

  integer, intent(in) :: i

  character           :: num(6)
  character*24        :: foldername, fname
  character*6         :: string
  
  num(1)='0'
  num(2)='0'
  num(3)='0'
  num(4)='0'
  num(5)='0'
  num(6)='0'

  if(i.lt.10) then
     
     write(string,100)i
100  format(I1)
     read(string,400) num(6)
400  format(6a1)
     
  else if(i.lt.100) then
     
     write(string,200)i
200  format(I2)
     read(string,400) num(5),num(6)
     
  elseif(i.lt.1000) then
     
     write(string,300)i
300  format(I3)
     read(string,400) num(4),num(5),num(6)
     
  elseif(i.lt.10000) then
     
     write(string,500)i
500  format(I4)
     read(string,400) num(3),num(4),num(5),num(6)
     
  elseif(i.lt.100000) then
     
     write(string,600)i
600  format(I5)
     read(string,400) num(2), num(3),num(4),num(5),num(6) 
     
  elseif(i.lt.1000000) then
     
     write(string,700)i
700  format(I6)
     read(string,400) num(1), num(2), num(3),num(4),num(5),num(6) 
     
  endif

  fname = '/pics/snp_'//num(1)//num(2)//num(3)//num(4)//num(5)//num(6)//'.out'

  return
end subroutine get_filename





subroutine get_foldername (i, fname)

  implicit none  

  integer, intent(in) :: i

  character           :: num(3)
  character*24        :: foldername, fname
  character*6         :: string
  
  num(1)='0'
  num(2)='0'
  num(3)='0'

  if(i.lt.10) then
     
     write(string,100)i
100  format(I1)
     read(string,400) num(3)
400  format(6a1)
     
  else if(i.lt.100) then
     
     write(string,200)i
200  format(I2)
     read(string,400) num(2),num(3)
     
  elseif(i.lt.1000) then
     
     write(string,300)i
300  format(I3)
     read(string,400) num(1),num(2),num(3)

  end if
  
  fname = 'run_'//num(1)//num(2)//num(3)

  return
end subroutine get_foldername





!--------------------------------------------------------------------------------------------
!- These two subroutines need to be edited if a different random no generator is being used

subroutine get_a_random_seed (seed)

  use glbl_vars, only : idnode
  implicit none

  integer               :: tim(8)
  integer, intent (out) :: seed

  !- Make this node dependent also

  call date_and_time ( values = tim )     ! Get the current time 
  seed = tim(4) * (360000*tim(5) + 6000*tim(6) + 100*tim(7) + tim(8)) 
  seed = seed + (idnode**3) + (idnode**2)
  seed = abs(seed)

  return
end subroutine get_a_random_seed





subroutine initialize_prng (seed)

  use mtprng
  use glbl_vars, only : state_ran

  implicit none

  integer, intent(in) :: seed
  
  call mtprng_init(seed, state_ran)

  return
end subroutine initialize_prng





subroutine measurement

  use glbl_vars
  use event_vars, only : global_time
  implicit none
  include 'mpif.h'

  integer                     :: ierr
  integer                     :: ipart
  integer                     :: nfree
  integer                     :: c_max
  integer, dimension(3)       :: data_packet
  integer, dimension(3)       :: data_cum
  real(kind=dp)               :: data_ener_cum

  imeas   = imeas + 1
  
  nfree = 0
  do ipart = 1, no_particles
     if (.not.colloids(ipart) % is_bonded) nfree = nfree + 1
  end do
  
  call get_largest_cluster (c_max)

  write ( f_fil2, '(i8)',      advance = 'no' ) nfree
  write ( f_fil3, '(i8)',      advance = 'no' ) nbonds
  write ( f_fil4, '(i8)',      advance = 'no' ) c_max
  write ( f_fil7, '(1es16.8)', advance = 'no' ) energy

  call MPI_BARRIER ( MPI_COMM_WORLD, ierr )
  
  data_packet (1) = nfree
  data_packet (2) = nbonds
  data_packet (3) = c_max
  
  call MPI_REDUCE ( data_packet, data_cum, 3, MPI_INTEGER , MPI_SUM, 0, MPI_COMM_WORLD, ierr )
  call MPI_REDUCE ( energy, data_ener_cum, 1, MPI_DOUBLE ,  MPI_SUM, 0, MPI_COMM_WORLD, ierr )

  if (idnode.eq.0) then  
     write ( g_fil1, '(1es16.8)', advance = 'no' ) global_time
     write ( g_fil2, '(1es16.8)', advance = 'no' ) dble(data_cum(1)) / dble(numnodes)
     write ( g_fil3, '(1es16.8)', advance = 'no' ) dble(data_cum(2)) / dble(numnodes)
     write ( g_fil4, '(1es16.8)', advance = 'no' ) dble(data_cum(3)) / dble(numnodes)
     write ( g_fil7, '(1es16.8)', advance = 'no' ) data_ener_cum     / dble(numnodes)     
  end if

  return
end subroutine measurement






subroutine add_newlines

  use glbl_vars
  implicit none
  include 'mpif.h'

  integer       :: inode, ierr
  integer       :: status(MPI_STATUS_SIZE)
  
  real(kind=dp) :: ent_recv
    
  if (idnode.eq.0) then
     
     write ( f_fil1, * ) ''
     write ( f_fil7, * ) ''
  
  end if

  call MPI_BARRIER ( MPI_COMM_WORLD, ierr )
  
  return
end subroutine add_newlines





subroutine end_simulation (ierr)

  use glbl_vars, only : idnode
  implicit none

  integer, intent(in) :: ierr
  integer             :: mpi_err

  call alloc_arrays(1)

  if ((idnode.eq.0).and.(ierr.eq.0)) then
     write(*,*) 'Wrong event_added'
  end if
  
  call MPI_FINALIZE(mpi_err)
  
  return
end subroutine end_simulation








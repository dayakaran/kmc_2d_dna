MODULE parse_module
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !
  ! dl_poly_4 module containing tools for parsing textual input
  !
  ! copyright - daresbury laboratory
  ! author    - i.t.todorov may 2004
  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  IMPLICIT NONE

  PUBLIC :: tabs_2_blanks, get_line, strip_blanks, lower_case, get_word, word_2_real

CONTAINS

  SUBROUTINE tabs_2_blanks(record)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !
    ! dl_poly_4 subroutine to convert tabs into blanks in a string
    !
    ! copyright - daresbury laboratory
    ! author    - i.t.todorov may 2010
    !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    IMPLICIT NONE

    CHARACTER( Len = * ), INTENT( InOut ) :: record

    INTEGER              :: i

    DO i=1,LEN_TRIM(record)
       IF (record(i:i) == ACHAR(9)) record(i:i) = ' '
    END DO

  END SUBROUTINE tabs_2_blanks

  SUBROUTINE get_line(safe,ifile,record)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !
    ! dl_poly_4 subroutine to read a character string on node zero and
    ! broadcast it to all other nodes
    !
    ! copyright - daresbury laboratory
    ! author    - i.t.todorov june 2011
    !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    IMPLICIT NONE

    LOGICAL,              INTENT(   Out ) :: safe
    INTEGER,              INTENT( IN    ) :: ifile
    CHARACTER( Len = * ), INTENT(   Out ) :: record

    INTEGER                              :: i,fail,rec_len, io_error
    INTEGER, DIMENSION( : ), ALLOCATABLE :: line

    rec_len = LEN(record)
    fail = 0
    ALLOCATE (line(1:rec_len), Stat = fail)

    record = ' '
    safe = .TRUE.

    READ(Unit=ifile, Fmt='(a)', IOSTAT = io_error) record
    IF (io_error.NE.0) safe = .false.

    DO i=1,rec_len
       line(i) = ICHAR(record(i:i))
    END DO
    
200 CONTINUE

    CALL tabs_2_blanks(record)
    DEALLOCATE (line, Stat = fail)

  END SUBROUTINE get_line



  SUBROUTINE strip_blanks(record)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !
    ! dl_poly_4 subroutine to strip blanks from either end of a string
    !
    ! copyright - daresbury laboratory
    ! author    - i.t.todorov july 2009
    !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    IMPLICIT NONE

    CHARACTER( Len = * ), INTENT( InOut ) :: record

    record = TRIM(ADJUSTL(record))

  END SUBROUTINE strip_blanks

  SUBROUTINE lower_case(record)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !
    ! dl_poly_4 subroutine to lower the character case of a string.
    ! Transportable to non-ASCII machines
    !
    ! copyright - daresbury laboratory
    ! author    - i.t.todorov june 2004
    !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    IMPLICIT NONE

    CHARACTER( Len = * ), INTENT( InOut ) :: record

    INTEGER :: i

    DO i=1,LEN(record)
       IF (record(i:i) == 'A') THEN
          record(i:i) = 'a'
       ELSE IF (record(i:i) == 'B') THEN
          record(i:i) = 'b'
       ELSE IF (record(i:i) == 'C') THEN
          record(i:i) = 'c'
       ELSE IF (record(i:i) == 'D') THEN
          record(i:i) = 'd'
       ELSE IF (record(i:i) == 'E') THEN
          record(i:i) = 'e'
       ELSE IF (record(i:i) == 'F') THEN
          record(i:i) = 'f'
       ELSE IF (record(i:i) == 'G') THEN
          record(i:i) = 'g'
       ELSE IF (record(i:i) == 'H') THEN
          record(i:i) = 'h'
       ELSE IF (record(i:i) == 'I') THEN
          record(i:i) = 'i'
       ELSE IF (record(i:i) == 'J') THEN
          record(i:i) = 'j'
       ELSE IF (record(i:i) == 'K') THEN
          record(i:i) = 'k'
       ELSE IF (record(i:i) == 'L') THEN
          record(i:i) = 'l'
       ELSE IF (record(i:i) == 'M') THEN
          record(i:i) = 'm'
       ELSE IF (record(i:i) == 'N') THEN
          record(i:i) = 'n'
       ELSE IF (record(i:i) == 'O') THEN
          record(i:i) = 'o'
       ELSE IF (record(i:i) == 'P') THEN
          record(i:i) = 'p'
       ELSE IF (record(i:i) == 'Q') THEN
          record(i:i) = 'q'
       ELSE IF (record(i:i) == 'R') THEN
          record(i:i) = 'r'
       ELSE IF (record(i:i) == 'S') THEN
          record(i:i) = 's'
       ELSE IF (record(i:i) == 'T') THEN
          record(i:i) = 't'
       ELSE IF (record(i:i) == 'U') THEN
          record(i:i) = 'u'
       ELSE IF (record(i:i) == 'V') THEN
          record(i:i) = 'v'
       ELSE IF (record(i:i) == 'W') THEN
          record(i:i) = 'w'
       ELSE IF (record(i:i) == 'X') THEN
          record(i:i) = 'x'
       ELSE IF (record(i:i) == 'Y') THEN
          record(i:i) = 'y'
       ELSE IF (record(i:i) == 'Z') THEN
          record(i:i) = 'z'
       END IF
    END DO

  END SUBROUTINE lower_case

  SUBROUTINE get_word(record,word)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !
    ! dl_poly_4 subroutine to transfer a word from a string
    !
    ! record loses a word and leading blanks
    ! word fills up with the word as much as it can contain
    !
    ! copyright - daresbury laboratory
    ! author    - i.t.todorov june 2004
    !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    IMPLICIT NONE

    CHARACTER( Len = * ), INTENT( InOut ) :: record
    CHARACTER( Len = * ), INTENT(   Out ) :: word

    LOGICAL :: transfer
    INTEGER :: rec_len,word_len,rec_ind,word_ind

    ! Strip blanks in record

    CALL strip_blanks(record)

    ! Get record and word lengths

    rec_len  = LEN_TRIM(record)
    word_len = LEN(word)

    ! Initialise counters and word, and keep-transferring boolean

    rec_ind  = 0
    word_ind = 0

    word     = ' '

    transfer = .TRUE.

    ! Start transferring

    DO WHILE (transfer)

       ! Check for end of record

       IF (rec_ind < rec_len) THEN

          rec_ind = rec_ind + 1

          ! Check for end of word in record

          IF (record(rec_ind:rec_ind) == ' ') transfer = .FALSE.

       ELSE

          transfer = .FALSE.

       END IF

       ! Transfer in word if there is space in word and transfer is true

       IF (word_ind < word_len .AND. transfer) THEN

          word_ind = word_ind + 1

          word(word_ind:word_ind) = record(rec_ind:rec_ind)

          record(rec_ind:rec_ind) = ' '

       ELSE

          ! Transfer to nothing if there is no space in word and transfer is true

          IF (transfer) record(rec_ind:rec_ind) = ' '

       END IF

    END DO

  END SUBROUTINE get_word

  FUNCTION word_2_real(word,def,report)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !
    ! dl_poly_4 function for extracting real numbers from a character string
    ! with no blanks between the characters of the number.  The optional
    ! argument 'def' suppresses error reporting to return a safe value
    !
    ! (1) Numbers as 2.0e-3/3.d-04 are processable as only one slash is
    !     permitted in the string!
    ! (2) Numbers cannot start or finish with a slash!
    ! (3) A blank string is read as zero!
    ! (4) Numbers must sensible!
    !
    ! copyright - daresbury laboratory
    ! author    - i.t.todorov november 2009
    !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    USE constants

    IMPLICIT NONE

    CHARACTER( Len = * ), INTENT( In    )           :: word
    REAL( Kind = dp ),    INTENT( In    ), OPTIONAL :: def
    LOGICAL,              INTENT( In    ), OPTIONAL :: report

    CHARACTER( Len = 40 ) :: forma
    LOGICAL               :: l_report = .TRUE.
    INTEGER               :: word_end,slash_position
    REAL( Kind = dp )     :: word_2_real,denominator

    IF (PRESENT(report)) l_report = report

    denominator = 1.0_dp

    word_end = LEN_TRIM(word)
    slash_position = INDEX(word,'/')

    IF (word_end /= 0) THEN
       IF (slash_position == 1 .OR. slash_position == word_end) Go To 30
    ELSE
       IF (PRESENT(def)) THEN
          word_2_real = def
       ELSE
          word_2_real = 0.0_dp
       END IF
       RETURN
    END IF

    IF (slash_position > 0) THEN
       forma = ' '
       WRITE(forma, 20) word_end - slash_position
       READ(word(slash_position + 1:word_end), forma, Err=30) denominator
       word_end = slash_position - 1
    END IF

    forma = ' '
    WRITE(forma,20) word_end
    READ(word(1:word_end), forma, Err=30) word_2_real
    word_2_real = word_2_real / denominator

    RETURN

20  FORMAT('(f',i0,'.0)')
30  CONTINUE
    IF (PRESENT(def)) THEN
       word_2_real = def
    ELSE
       word_2_real = 0.0_dp
    END IF

  END FUNCTION word_2_real

  FUNCTION truncate_real(r)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !
    ! dl_poly_4 function for truncating real numbers to the approximate
    ! precision in decimal digits for the +/-0.___E+/-___ representation,
    ! which is 2*Bit_Size(real)-1
    !
    ! copyright - daresbury laboratory
    ! author    - i.t.todorov october 2005
    !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    USE constants

    IMPLICIT NONE

    REAL( Kind = dp ), INTENT( In    ) :: r

    LOGICAL               , SAVE :: newjob = .TRUE.
    CHARACTER( Len = 40  ), SAVE :: forma  = ' '
    INTEGER               , SAVE :: k      = 0

    CHARACTER( Len = 100 ) :: word
    INTEGER                :: e_position,word_end,i
    REAL( Kind = dp )      :: truncate_real

    IF (newjob) THEN
       newjob = .FALSE.

       k = 64/4 - 1! Bit_Size(0.0_dp)/4 - 1

       WRITE(forma ,10) k+10,k
10     FORMAT('(0p,e',i0,'.',i0,')')
    END IF

    word = ' '
    WRITE(word,forma) r
    CALL lower_case(word)
    word_end = LEN_TRIM(word)
    e_position = 0
    e_position = INDEX(word,'e')
    DO i=e_position-3,word_end
       IF (i+3 <= word_end) THEN
          word(i:i)=word(i+3:i+3)
       ELSE
          word(i:i)=' '
       END IF
    END DO

    CALL strip_blanks(word)
    truncate_real=word_2_real(word)

  END FUNCTION truncate_real

END MODULE parse_module

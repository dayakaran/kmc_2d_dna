subroutine init_simulation

  use glbl_vars
  implicit none  
  
  ! Nothing to do here

  open ( unit=f_info, file='siminfo.dat', status='unknown' )

  call system("mkdir pics")

  call make_files
  
  return  
end subroutine init_simulation



subroutine make_files 

  use glbl_vars
  implicit none  

  !integer, intent(in) :: i

  open ( unit=f_fil1, file='time.dat',   status='unknown' )
  open ( unit=f_fil2, file='nfree.dat',  status='unknown' )
  open ( unit=f_fil3, file='nlinks.dat', status='unknown' )
  open ( unit=f_fil4, file='c_max.dat',  status='unknown' )
  open ( unit=f_fil7, file='energy.dat', status='unknown' )

  return
end subroutine make_files



subroutine open_vis_file(rid)

  use glbl_vars, only : f_fil8
  implicit none

  integer, intent(out) :: rid
  integer, save        :: id = 0

  character(len=24)    :: fname
  
  id  = id + 1

  call get_filename(id, fname)
  open ( unit=f_fil8, file=fname,   status='unknown' )
  rid = f_fil8
  
  return
end subroutine open_vis_file

!--------------------------------------------------------------------------------------------
!- These two subroutines need to be edited if a different random no generator is being used

subroutine get_a_random_seed (seed)

  implicit none

  integer               :: tim(8)
  integer, intent (out) :: seed

  !- Make this node dependent also

  call date_and_time ( values = tim )     ! Get the current time 
  seed = tim(4) * (360000*tim(5) + 6000*tim(6) + 100*tim(7) + tim(8)) 
  seed = abs(seed)
 
  return
end subroutine get_a_random_seed





subroutine initialize_prng (seed)

  use mtprng
  use glbl_vars, only : state_ran

  implicit none

  integer, intent(in) :: seed
  
  call mtprng_init(seed, state_ran)

  return
end subroutine initialize_prng




subroutine measurement

  use glbl_vars
  use event_vars, only : global_time
  implicit none

  integer :: nfree, ipart, c_max

  nfree = 0
  do ipart = 1, no_particles
     if (.not.colloids(ipart) % is_bonded) nfree = nfree + 1
  end do

  call get_largest_cluster (c_max)
    
  write ( f_fil1, '(1es16.8)', advance = 'no' ) global_time
  write ( f_fil2, '(i8)',      advance = 'no' ) nfree
  write ( f_fil3, '(i8)',      advance = 'no' ) nbonds
  write ( f_fil4, '(i8)',      advance = 'no' ) c_max
  write ( f_fil7, '(1es16.8)', advance = 'no' ) energy
  
  imeas   = imeas + 1

  return
end subroutine measurement




subroutine add_newlines

  use glbl_vars
  implicit none
  
  write ( f_fil1, * ) ''
  write ( f_fil2, * ) ''
  write ( f_fil3, * ) ''
  write ( f_fil7, * ) ''

  flush ( f_fil1 )
  flush ( f_fil2 )
  flush ( f_fil3 )
  flush ( f_fil7 )

  return
end subroutine add_newlines




subroutine end_simulation (ierr)

  implicit none
  integer, intent(in) :: ierr

  call alloc_arrays(1)

  if (ierr.eq.0) then     
     write(*,*) 'Wrong event_added'
  end if

  return
end subroutine end_simulation





subroutine get_filename (i, fname)

  use glbl_vars
  implicit none  

  integer, intent(in) :: i

  character           :: num(6)
  character*24        :: foldername, fname
  character*6         :: string
  
  num(1)='0'
  num(2)='0'
  num(3)='0'
  num(4)='0'
  num(5)='0'
  num(6)='0'

  if(i.lt.10) then
     
     write(string,100)i
100  format(I1)
     read(string,400) num(6)
400  format(6a1)
     
  else if(i.lt.100) then
     
     write(string,200)i
200  format(I2)
     read(string,400) num(5),num(6)
     
  elseif(i.lt.1000) then
     
     write(string,300)i
300  format(I3)
     read(string,400) num(4),num(5),num(6)
     
  elseif(i.lt.10000) then
     
     write(string,500)i
500  format(I4)
     read(string,400) num(3),num(4),num(5),num(6)
     
  elseif(i.lt.100000) then
     
     write(string,600)i
600  format(I5)
     read(string,400) num(2), num(3),num(4),num(5),num(6) 
     
  elseif(i.lt.1000000) then
     
     write(string,700)i
700  format(I6)
     read(string,400) num(1), num(2), num(3),num(4),num(5),num(6) 
     
  endif

  fname = 'pics/snp_'//num(1)//num(2)//num(3)//num(4)//num(5)//num(6)//'.out'

  return
end subroutine get_filename


! subroutine vis

!   use mof_var
!   use glbl_vars, only : f_vis, nmax_sites, idnode
!   implicit none

!   integer :: isite

!   do isite = 1, nmax_sites
!      write(f_vis,'(i2)', advance='no') mof_sites (isite) % my_type
!   end do
!   write(f_vis,*) ''
  
!   flush(f_vis)

!   return
! end subroutine vis

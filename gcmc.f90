subroutine mc_equil

  use glbl_vars 
  implicit none

  integer        :: isweep, rdof
  integer        :: j

  real(kind=dp)  :: r
  
  !-----------------------------------------------------------------------
  !~ Let s synchronize the threads before we start the calculation

  do isweep = 1,nsweep
   
     do j = 1, no_particles + nlinks
        
        call rngu(r)
        rdof = int(r*(no_particles+nlinks)) + 1

        if (rdof.le.no_particles) then
           call translate_particle (rdof)
        else
           call flip_link_unbiased (rdof - no_particles)
        end if

     end do

  end do

  return
end subroutine mc_equil








subroutine flip_link_unbiased ( rlink )

  use rate_vars, only : tstar, eps_bond
  use glbl_vars
  implicit none

  integer, intent(in) :: rlink

  integer             :: snn, slink, dlink
  integer             :: ipart, jpart, k
  integer             :: socc,nocc, isite, jsite

  real(kind=dp)       :: boltz, r

  isite  = llist(1, rlink)
  jsite  = llist(2, rlink)
  
  slink  = links ( rlink )
  dlink  = 1 - (2*slink)
  
  socc   = occ(isite) 
  nocc   = occ(jsite)

  !- This entire energy calculation routine has to change 

  if (slink.eq.0) then

     if ( (socc.eq.1).and.(nocc.eq.1) ) then
        boltz = exp(eps_bond/tstar)
     else
        boltz = 0.0_dp
        return
     end if

  else
     
     boltz = exp(-eps_bond/tstar)
     
  end if

  call rngu(r) 
  if (r.lt.boltz) then           
        
     links (rlink) = 1 - slink
     energy        = energy + ( eps_bond * dlink )
     nbonds        = nbonds + dlink

     !- Change bonding status of particles
     !- Get the name of the two particles involved

     ipart         = tag(isite)
     jpart         = tag(jsite)
    
     if (slink.eq.0) then
        
        colloids (ipart) % is_bonded = .true.
        colloids (jpart) % is_bonded = .true.
        
     else

        snn = 0
        do k = 1, nc
           snn = snn + links (slmap (k, isite))
        end do
        if (snn.eq.0) colloids (ipart) % is_bonded = .false.

        snn = 0
        do k = 1, nc
           snn = snn + links (slmap (k, jsite))
        end do
        if (snn.eq.0) colloids (jpart) % is_bonded = .false.
        
     end if
     
  end if
  
  return
end subroutine flip_link_unbiased






subroutine translate_particle (rpart)

  use rate_vars, only : tstar, eps_bond
  use glbl_vars
  implicit none

  integer, intent(in) :: rpart

  integer             :: rsite
  integer             :: tsite

  real(kind=dp)       :: r, boltz

  if (colloids(rpart) % is_bonded) return


  rsite = colloids (rpart) % site
  
  call rngu(r)
  tsite = int(r*nsite)+1
     
  if (occ(tsite).eq.0) then
     boltz = 1.0_dp
  else
     boltz = 0.0_dp
     return
  end if
  
  !call rngu(r) ! Rates 0 or 1 so no need to compute Boltzmann weights
  !if (boltz.gt.r) then
  
  occ (rsite) = 0
  occ (tsite) = 1
  
  tag (rsite) = 0
  tag (tsite) = rpart
  
  colloids (rpart) % site = tsite
     
  !end if

  return
end subroutine translate_particle




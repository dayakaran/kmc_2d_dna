module kinds_f90

  implicit none
  save

  integer, parameter :: INT64 = selected_int_kind(18)
  integer, parameter :: INT32 = selected_int_kind(9)
  integer, parameter :: INT16 = selected_int_kind(4)
  integer, parameter :: INT08 = selected_int_kind(2)
  
  integer, parameter :: dp = SELECTED_REAL_kind (15, 307)
  integer, parameter :: li = SELECTED_INT_kind (12)
  integer, parameter :: sp = selected_real_kind( 6, 37 )
  
  
end module kinds_f90

module constants

  use kinds_f90
  implicit none
 
  real(kind = dp), parameter :: pi = 3.141592653589793_dp
  real(kind = dp), parameter :: twopi  =  6.28318530717958623e0_dp ! 4.0_wp*Asin(1.0_wp)
  real(kind = dp), parameter :: fourpi = 12.56637061435917246e0_dp ! 8.0_wp*Asin(1.0_wp)
  real(kind = dp), parameter :: sqrpi  =  1.77245385090551588e0_dp ! Sqrt(pi)
  real(kind = dp), parameter :: rtwopi =  0.15915494309189535e0_dp ! 1.0_wp/twopi
  real(kind = dp), parameter :: pi_by_six = 0.5235987760_dp
  real(kind = dp), parameter :: pi_by_three = pi / 3.0_dp
  real(kind = dp), parameter :: pi_by_four = pi / 4.0_dp
  real(kind = dp), parameter :: pi_by_two  = pi / 2.0_dp
  real(kind = dp), parameter :: degrad = pi/180.0_dp, raddeg = 180.0_dp/pi
  real(kind = dp), parameter :: two_by_three = 2.0_dp/3.0_dp
  real(kind = dp), parameter :: inv_sqrt_2 = 1.0d0/sqrt(2.0_dp) 
  
end module constants



! ***************************************************************
! KMC scheme for DNA bound particles
! Original code - John Jackass Edison
! 2D Lattice Gas
! ****************************************************************

program mc_main
  
  use glbl_vars
  use rate_vars
  implicit none

  integer :: ierr

  call init_simulation
  call loadparam
  call run_constants
  call alloc_arrays(0)

  call lattice

  call init_solvent
  call estimate_rates
  call mc_equil !- Check if this subroutine works
  call compute_energy
  
  call build_future_events_calender
  
  call kmc
  call end_simulation(1)

  stop  
end program mc_main




subroutine alloc_arrays(alloc_in)
  
  use glbl_vars
  use event_vars
  implicit none
  
  integer, intent(in) :: alloc_in
  integer             :: all_error
  
  if (alloc_in.eq.0) then
     
     allocate ( ilist(nsite)           ,stat = all_error )  
     allocate ( ix(nsite)              ,stat = all_error )     
     allocate ( iy(nsite)              ,stat = all_error )     

     allocate ( occ(nsite)             ,stat = all_error )
     allocate ( tag(nsite)             ,stat = all_error )
     allocate ( colloids(no_particles) ,stat = all_error )
     allocate ( links(nlinks)          ,stat = all_error )
     
     allocate ( nlist(nc,nsite)        ,stat = all_error )
     allocate ( llist(nc/2,nlinks)     ,stat = all_error )
     allocate ( slmap(nc,nlinks)       ,stat = all_error )
     
     allocate ( binary_tree    ( size_binary_tree )      )
     allocate ( event_list     ( size_event_list  )      )
     allocate ( track_hops_ze  ( nmax_hop_events )       )
     allocate ( track_links_ze ( nlinks )                )
     
     ilist = 0
     ix    = 0
     iy    = 0
     occ   = 0
     tag   = 0
     
     nlist = 0
     links = 0
     llist = 0
     slmap = 0
     
  else
     
     deallocate ( ilist )
     deallocate ( ix )
     deallocate ( iy )
     deallocate ( occ )
     deallocate ( tag )
     deallocate ( colloids )

     deallocate ( nlist )
     deallocate ( links )
     deallocate ( llist )
     deallocate ( slmap )
     
     deallocate ( binary_tree )
     deallocate ( event_list )
     deallocate ( track_hops_ze )
     deallocate ( track_links_ze )
     
  end if

  return
end subroutine alloc_arrays








subroutine rngu(r)

  use kinds_f90
  use glbl_vars, only : state_ran
  use mtprng
  implicit none

  real(kind = dp), intent(OUT) :: r
  !real(kind = dp), external :: randf

  r = mtprng_rand_real2 (state_ran)

  return
end subroutine rngu





function randf(idum)

  implicit none
  integer, parameter :: k4b=SELECTED_INT_kind(9)
  integer(k4b), intent(inout) :: idum
  real(kind = 8) :: randf
  integer(k4b), parameter :: ia=16807,im=2147483647,iq=127773,ir=2836
  real(kind = 8), save :: am
  integer(k4b), save :: ix=-1,iy=-1,k

  if (idum <= 0 .or. iy < 0) then
     am=nearest(1.0,-1.0)/im
     iy=ior(ieor(888889999,abs(idum)),1)
     ix=ieor(777755555,abs(idum))
     idum=abs(idum)+1

  end if

  ix=ieor(ix,ishft(ix,13))
  ix=ieor(ix,ishft(ix,-17))
  ix=ieor(ix,ishft(ix,5))

  k=iy/iq
  iy=ia*(iy-k*iq)-ir*k
  if (iy < 0) iy=iy+im
  randf=am*ior(iand(im,ieor(ix,iy)),1)

end function randf






function exp_rn ( scale )

  use kinds_f90
  use glbl_vars, only : state_ran
  use mtprng
  implicit none

  real(kind = dp), intent(in) :: scale
  real(kind = dp)             :: exp_rn, r

  r      = mtprng_rand_real2 (state_ran)
  exp_rn = log(1.0_dp-r) / (-scale)

  return
end function exp_rn

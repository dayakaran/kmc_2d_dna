module US_variables

  use constants
  implicit none
  save

  integer                                    :: max_OP
  real(kind = dp), dimension(:), allocatable :: hist_OP, hist_OP_global

end module US_variables



subroutine umbrella_sampling

  use glbl
  use tmmc_var
  use US_variables
  include 'mpif.h'

  integer :: ierr

  max_OP          = nsite

  allocate (hist_OP        (0:max_OP))
  allocate (hist_OP_global (0:max_OP))

  hist_OP         = 0.0_dp
  hist_OP_global  = 0.0_dp

  call MPI_Barrier(MPI_COMM_WORLD,ierr)
  call mpi_bcast  (LPIN_global, nmax_occupancy+1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)

  do i = 0, nmax_occupancy
     eta_global(i) = -LPIN_global(i)
  end do

  call run_umbrella_sampling

  return
end subroutine umbrella_sampling




subroutine run_umbrella_sampling
  
  use glbl
  use tmmc_var
  use US_variables
  implicit none
  include 'mpif.h'

  integer        :: snn, n_E_index
  integer        :: isweep,j,rsite,socc,k,docc,nesite
  integer        :: is,ierr
  integer        :: w_sta, w_end, w_rng

  real(kind=dp)  :: r, rhosys, boltz, delE, avg_bias, avg_bias_global

  !-----------------------------------------------------------------------
  !~ Let s synchronize the threads before we start the calculation

  avg_bias = 0.0_dp

  call MPI_Barrier(MPI_COMM_WORLD,ierr)

  do isweep = 1,nsweep
     do j = 1,nsite
        
        call rngu(r)
        rsite = int(r*nsite) + 1
        socc = occ(rsite)   
        
        if ((socc.eq.1).and.(nfluid.eq.nfluid_min)) then
            cycle
        elseif ((socc.eq.0).and.(nfluid.eq.nfluid_max)) then
            cycle
        end if
        
        docc = 1-(2*socc)           
        snn = 0
           
        do k = 1,nc
              
           nesite = nlist(k,rsite)              
           snn = snn + occ(nesite)
              
        end do

        delE  = dble(-docc) * snn  
        boltz = boltz_factors(socc, snn)        
        boltz = boltz*(exp(eta(nfluid + docc) - eta(nfluid)))

        call rngu(r)
        if (r.lt.boltz) then           

           occ(rsite) = 1 - occ(rsite)  
           nfluid = nfluid + docc
           energy = energy + delE           
           
        end if
        
     end do
     
     call count_clusters()
     avg_bias = avg_bias + exp(-eta_global(nfluid))

  end do     

  call MPI_Barrier ( MPI_COMM_WORLD,ierr )
  call MPI_Reduce  ( hist_OP,   hist_OP_global, max_OP + 1, MPI_DOUBLE_PRECISION, MPI_SUM, 0, MPI_COMM_WORLD, ierr )
  call MPI_Reduce  ( avg_bias, avg_bias_global,          1, MPI_DOUBLE_PRECISION, MPI_SUM, 0, MPI_COMM_WORLD, ierr )
  call MPI_Barrier ( MPI_COMM_WORLD,ierr )

  if (idnode.eq.0) then

     open(unit=101,file='hofn.dat',status='unknown')
     write(101, '(a12,a16)') 'OP','H(OP)'
     do j = 0, nsite
        hist_OP_global(j) = hist_OP_global(j) / avg_bias_global
        write(101,'(i12,1es16.8)') j, hist_OP_global(j)    
     end do
     close(101)

  end if
  
  call MPI_Barrier(MPI_COMM_WORLD,ierr)

  return
end subroutine run_umbrella_sampling





subroutine count_clusters()

  use glbl
  use tmmc_var
  use US_variables
  implicit none

  integer                 :: largest_cluster_size
  integer                 :: rsite
  integer                 :: stack(nsite)
  integer                 :: stack_count
  integer                 :: ineighbor, nesite
  integer                 :: socc, docc, nocc, csite, isolid_buffer
  integer                 :: snn_count, phi_count
  integer                 :: clustersize
  logical                 :: searched (nsite)

!!$  prob_add    = 1.0_dp 

  searched             = .false.
  largest_cluster_size = 0
  if (nfluid.gt.0) largest_cluster_size = 1

  do rsite = 1, nsite

     if (occ(rsite).eq.0) cycle
     if (searched(rsite)) cycle

     stack           = 0
     stack(1)        = rsite
     stack_count     = 1
     searched(rsite) = .true.
     clustersize     = 1

     do 

        if (stack_count.eq.0) exit

        csite       = stack(stack_count)
        stack_count = stack_count - 1

        do ineighbor = 1, nc

           nesite = nlist (ineighbor, csite)
           if (isolid(nesite).eq.0) then
              if (occ(nesite).eq.1) then
                 if (.not.searched(nesite)) then
                    stack_count        = stack_count + 1
                    stack(stack_count) = nesite
                    clustersize        = clustersize + 1
                    searched(nesite)   = .true.
                 end if
              end if
           end if
        end do
     end do

     hist_OP(clustersize) = hist_OP(clustersize) + (exp(-eta_global(nfluid)))
     
     if (clustersize.gt.largest_cluster_size) then
        largest_cluster_size = clustersize
     end if

  end do

  !hist_OP(largest_cluster_size) = hist_OP(largest_cluster_size) + (exp(-eta_global(nfluid)))

  return
end subroutine count_clusters






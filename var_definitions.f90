!~ List of variables

!~ nsite            = Total no. of sites
!~ sweeps           = No of GCMC sweeps
!~ nupdate          = No. of sweeps before biasing function in updated
!~ npart            = No. of filled sites in the system
!~ occ              = Occupancy array of the system
!~ bound_con        = Boundary condition 0 - cubic periodic 1 - Slab for confinement
!~ npic             = Frequency at which snapshots are printed out
!~ nsolid           = Total no of solid sites
!~ npr_pofn         = Frequency at which P(N) are printed out

!~ Variables used for the TMMC method

!~ C          = C is the tri banded collection matrix
!~ nmax       = Max. no of particles
!~ nmin       = Min. no of particles
!~ P(N)       = Probability distribution matrix
!~ h(N,E)     = Frequency of observing the system at energy E at a particle number N
!~ f(N,E)     = Normalized version of h(N,E)
!~ karbit     = Arbitrary initial value for biasing function
!~ n_E_levels = Total number of energy levels in the system

!~ The first dimension in C is of size 3, index 1 for delete
!~                                        index 2 for no change
!~                                        index 3 for no create

module glbl_vars
  
  use kinds_f90
  use mtprng
  implicit none
  save

  integer, parameter                        :: nc = 4, f_info = 301
  integer, parameter                        :: f_fil1 = 401, f_fil2 = 402, f_fil3 = 403, f_fil4 = 404
  integer, parameter                        :: f_fil5 = 405, f_fil6 = 406, f_fil7 = 407, f_fil8 = 408

  integer, parameter                        :: g_fil1 = 601, g_fil2 = 602, g_fil3 = 603, g_fil4 = 604
  integer, parameter                        :: g_fil5 = 605, g_fil6 = 606, g_fil7 = 607, g_fil8 = 608

  type, public :: particles  
     integer         :: site = 0
     logical         :: is_bonded = .false.
  end type particles
  
  integer                                   :: length, height 
  integer                                   :: npic, ifile, nmax_occupancy
  integer                                   :: nsite, no_particles, nlinks, nbonds, nsweep
  integer                                   :: numnodes, idnode, imeas = 0
  integer                                   :: f_vis, f_cchart, f_hofn

  real(kind = dp)                           :: energy
  real(kind = dp)                           :: avg_rho
  real(kind = dp)                           :: avg_ene

  
  integer, dimension(:),   allocatable      :: ilist, ix, iy, occ, links, tag
  integer, dimension(:,:), allocatable      :: nlist, llist, slmap
    
  character*2                               :: t(3)

  type(particles),dimension(:), allocatable :: colloids
  type(mtprng_state)                        :: state_ran
  
end module glbl_vars


module event_vars

  use kinds_f90
  implicit none
  save

  integer, parameter :: EVENT_HOP_NEI = 1

  integer, parameter :: EVENT_BND_CRE = 11
  integer, parameter :: EVENT_BND_DEL = 12 

  integer, parameter :: EVENT_TYPE_MEAS = 51
  integer, parameter :: EVENT_TYPE_KLIK = 52
  integer, parameter :: EVENT_TYPE_HALT = 53
  integer, parameter :: EVENT_TYPE_FILD = 54

  type, public :: events
     
     integer         :: hero
     integer         :: inf1
     integer         :: inf2
     integer         :: event_type
     logical         :: is_stuck
     real(kind = dp) :: event_rate
     real(kind = dp) :: event_time
     
  end type events
  
  type, public :: tracker
     
     real(kind = dp) :: rate
     real(kind = dp) :: tau
     real(kind = dp) :: time
     logical         :: is_virgin

  end type tracker

  integer                                  :: size_event_list
  integer                                  :: size_binary_tree
  integer                                  :: no_system_events = 4
  integer                                  :: nmax_hop_events
  integer                                  :: nmax_link_events
  
  real(kind = dp)                          :: global_time = 0.0_dp

  type(events),  dimension(:), allocatable :: event_list
  type(events),  dimension(:), allocatable :: binary_tree
  type(tracker), dimension(:), allocatable :: track_links_ze, track_hops_ze

end module event_vars




module rate_vars

  use kinds_f90
  implicit none
  save
  
  real(kind = dp) :: E_bind = 2.0_dp
  real(kind = dp) :: tstar = 1.0_dp
  real(kind = dp) :: eps_bond = 1.0_dp

  real(kind = dp) :: rate_hop            = 1.0_dp
  real(kind = dp) :: rate_bond_formation = 1.0_dp
  real(kind = dp) :: rate_bond_deletion  = 1.0_dp
  real(kind = dp) :: damkohler_no        = 1.0_dp
  
  real(kind = dp) :: next_selfie_time
  real(kind = dp) :: next_measur_time
  real(kind = dp) :: next_halt_time
  real(kind = dp) :: next_field_adjust_time

  real(kind = dp), parameter :: INF_TIME = 1000000000000.0_dp
  
contains

  subroutine estimate_rates 

    implicit none

    real (kind = dp) :: beta_E_b1
    
    rate_hop            = 1.0_dp       !- Time scale of hops
    rate_bond_formation = damkohler_no ! * rate_hop which is always fixed at 1.0
    rate_bond_deletion  = rate_bond_formation*exp ((-eps_bond)/tstar) 
    
    return
  end subroutine estimate_rates
  
end module rate_vars

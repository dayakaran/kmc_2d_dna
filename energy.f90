subroutine compute_energy

  use glbl_vars
  use rate_vars
  implicit none

  integer :: ilink

  energy = 0.0_dp   

  do ilink = 1, nlinks
     energy = energy - ( eps_bond * dble(links(ilink)) ) 
  end do
  

  return
end subroutine compute_energy






subroutine get_largest_cluster ( largest_cluster_size )

  use glbl_vars
  implicit none

  integer, intent(out)    :: largest_cluster_size
  integer                 :: rsite
  integer                 :: stack(nsite)
  integer                 :: stack_count
  integer                 :: ineighbor, nesite
  integer                 :: socc, docc, nocc, csite
  integer                 :: clustersize
  logical                 :: searched (nsite)

!!$  prob_add    = 1.0_dp 

  searched             = .false.
  largest_cluster_size = 0
  if (no_particles.gt.0) largest_cluster_size = 1

  do rsite = 1, nsite

     if (occ(rsite).eq.0) cycle
     if (searched(rsite)) cycle

     stack           = 0
     stack(1)        = rsite
     stack_count     = 1
     searched(rsite) = .true.
     clustersize     = 1

     do 

        if (stack_count.eq.0) exit

        csite       = stack(stack_count)
        stack_count = stack_count - 1

        do ineighbor = 1, nc

           nesite = nlist (ineighbor, csite)
           if (occ(nesite).eq.1) then
              if (links(slmap ( ineighbor, csite)).eq.1) then  !- All sites with links are added to cluster
                 if (.not.searched(nesite)) then
                    stack_count        = stack_count + 1
                    stack(stack_count) = nesite
                    clustersize        = clustersize + 1
                    searched(nesite)   = .true.
                 end if
              end if
           end if
        end do
     end do

     if (clustersize.gt.largest_cluster_size) then
        largest_cluster_size = clustersize
     end if

  end do

  return
end subroutine get_largest_cluster
